# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Organize into proper Python module
- `-o` flag that generates #ifdef macro based on filename (`cppheader` target only)
- Namespaces for C++ header file output
- New output languages
- Alternative input formats
- Checking for uniqueness of integers
- Support for string key.

## [0.0.1] - 2023-03-06

### Added

- Initial version with support for CSV based input and C++ output (both plain C++ and header file)
