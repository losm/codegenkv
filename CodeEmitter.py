from datetime import date
import sys


class CodeEmitter:
    def __init__(self, app_info):
        self.name = app_info[0]
        self.version = app_info[1]
        today = date.today()
        self.date = "{}-{:02d}-{:02d}".format(today.year, today.month,
                                              today.day)

    def Prologue(self):
        sys.exit("FATAL: Prologue is a pure virtual function.")

    def Content(self, entry):
        sys.exit("FATAL: Content is a pure virtual function.")

    def Epilogue(self):
        sys.exit("FATAL: Epilogue is a pure virtual function.")
