import CodeEmitter

from CodeEmitter import CodeEmitter


class CppEmitter(CodeEmitter):
    def __init__(self, app_info):
        super().__init__(app_info)

    def Prologue(self):
        print("std::map<int, std::string> mymap = {")

    def Content(self, entry, is_first):
        if (not is_first):
            print(",", end="")
        print("{{ {},\"{}\"}}".format(entry[0], entry[1]))

    def Epilogue(self):
        print("};")
