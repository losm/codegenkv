# Code Generator for KV-Pairs

Utility for generating source code for key-value based lookup tables.

Takes a CSV file as an input and generates a string lookup table indexed by an integer value.

Assumes unique integers.

## Usage

```
usage: CodeGenKV.py [-h] [--version] -i CSV input_filename -t {cpp,cppheader} [--ifdef-macro IFDEF_MACRO]

Generates Key-Value lookup table source code from CSV files.

options:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -i CSV input_filename
  -t {cpp,cppheader}    'cpp': generate plain C++ code, 'cppheader': generate C++ header file
  --ifdef-macro IFDEF_MACRO
                        Specifies the #ifdef macro. Required for the 'cppheader' output target.

```