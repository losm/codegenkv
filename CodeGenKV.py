#!/usr/bin/env python

import argparse
import csv
import os
import sys

from CppEmitter import CppEmitter
from CppFileEmitter import CppFileEmitter


version = "0.0.1"
prog_name = os.path.basename(__file__)


def startup() -> bool:
    global prog
    global emitter
    global input_filename
    global ifdef_macro
    global emitter

    parser = argparse.ArgumentParser(
        prog=prog_name,
        description="""Generates Key-Value lookup table
                       source code from CSV files.""")

    parser.add_argument("--version", action="version", version=version)

    parser.add_argument("-i", required=True, action="append",
                        metavar="CSV input_filename",
                        type=str)
    parser.add_argument("-t", required=True, choices=["cpp", "cppheader"],
                        type=str,
                        help="""
                        'cpp': generate plain C++ code, 'cppheader':
                        generate C++ header file""")
    parser.add_argument("--ifdef-macro", action="append",
                        help="""Specifies the #ifdef macro. Required for
                                the 'cppheader' output target.""")

    args = parser.parse_args()

    input_filename = args.i[0]
    target = args.t
    app_info = (prog_name, version)
    match target:
        case "cpp":
            emitter = CppEmitter(app_info)
        case "cppheader":
            if (args.ifdef_macro is None):
                sys.exit("""'ifdef-macro' must be specified when
                         generating a header file.""")
            emitter = CppFileEmitter(app_info, args.ifdef_macro[0])


def generate(input_filename):
    try:
        with open(input_filename) as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            emitter.Prologue()
            is_first = True
            for row in reader:
                emitter.Content(row, is_first)
                if is_first:
                    is_first = False
            emitter.Epilogue()
    except FileNotFoundError:
        print(input_filename + " not found.")
        sys.exit(1)
    except IndexError:
        print("Internal error: cannot parse CSV.")
        sys.exit(2)


if __name__ == "__main__":
    startup()
    generate(input_filename)
